// This is the data for the weather API key
var apikeyW = 'appid=4650abaebf2196f3ebcea5ef12935228'

// API perameters for weather call
var units = "units=imperial";
var lat;
var lon;

// Global variable to hold some data
var theDataL
var theDataW
var theData5

// API url for location call
let geoUrl = 'http://api.ipstack.com/check?access_key=84f5f0552bfec0e672063cec5976e983'

// Returns the parameter String for the GET request
var formParamsString = function() {
    let argsArray = Array.from(arguments);
    let url = argsArray.shift();
    return `${url}?${argsArray.join('&')}`;
}

var app = new Vue({
                  el: '#rain',
                  data: {
                  
                  weather: [ ],
                  five: [ ],
                  current: [ {temp: 0, hum: 0, pres: 0, sky: ""},],
                  geo: [ ],
                  },
                  methods: {
                  
                  
                  toggle: function(ev) {
                            let idx = ev.target.getAttribute('data-idx');
                            let temp = this.weather;
                            let cls = ev.target.getAttribute('class');
                            if (cls === 'shadowed blue stuff-box'){
                                ev.target.setAttribute('class','shadowed green stuff-box')
                                temp[idx].state = 'good';
                                console.log(this.weather[idx].state);

                                }
                            else if (cls === 'shadowed green stuff-box'){
                                ev.target.setAttribute('class', 'shadowed red stuff-box')
                                temp[idx].state = 'bad';
                                console.log(this.weather[idx].state);


                                }
                            else{
                                ev.target.setAttribute('class', 'shadowed blue stuff-box')
                                temp[idx].state = 'normal';
                                console.log(this.weather[idx].state);
                                }
                  this.weather = temp;
                            },
                  },
                  
                  computed: {
                  
                  
                  
                  good : function() {
                  let g = 0;
                  
                  for (item of this.weather ){
                  
                  let cls = item.state;
                  if (cls === 'good') {console.log("checking for mornal"); g++;}
                  }
                  return g;
                  },
                  
                  bad : function() {
                  let b = 0;
                  for (item of this.weather ){
                  
                  let cls = item.state;
                  if (cls === 'bad'){ console.log("checking for bad");b++;}
                  }
                  return b;
                  },
                  
                  normal : function() {
                    let n = 0;
                    for (item of this.weather ){

                        let cls = item.state;
                  if (cls === 'normal'){ console.log("checking for normal"); n++;}
                    }
                        return n;
                  },
                  
                  
                  },
                  
                  // This is the start of the API calls. The location API is called
                  // first and then the weather data uses the info from the first call
                  // in its fetch()
                  created() {
                  fetch(geoUrl)
                  .then( r => r.json() )
                  .then( json => {
                        console.log(json);
                        this.geo = json;
                        
                        var lat = this.geo.latitude;
                        var lon = this.geo.longitude;
                        
                        var latitude = `lat=${lat}`;
                        var longitude = `lon=${lon}`;
                        
                        let urlWeather = formParamsString('http://api.openweathermap.org/data/2.5/weather', latitude, longitude, units, apikeyW);
                        
                        
                        return fetch(urlWeather)  })
                  .then( r => r.json() )
                  .then( json => {
                        console.log(json);
                        this.current = json;
                        var curweather = this.current;
                        console.log(curweather);
                        
                        this.current.temp = curweather.main.temp;
                        console.log(this.current.temp);
                        
                        this.current.hum = curweather.main.humidity;
                        console.log(this.current.hum);
                        
                        this.current.pres = curweather.main.pressure;
                        console.log(this.current.pres);
                        
                        this.current.sky = curweather.weather[0].main;
                        console.log(this.current.sky);
                        
                        
                        var zip = `zip=${this.geo.zip}`;
                        
                        var lat = this.geo.latitude;
                        var lon = this.geo.longitude;
                        
                        var latitude = `lat=${lat}`;
                        var longitude = `lon=${lon}`;
                        
                        let url5day = formParamsString('http://api.openweathermap.org/data/2.5/forecast', latitude, longitude, units, apikeyW);
                        
                        
                        return fetch(url5day) })
                  .then( r => r.json() )
                  .then( json => {
                        console.log(json);
                        this.five = json;
                        this.weather = this.five.list;
                        for (i = 0; i < 40; i++){
                        this.weather[i].newAttribute = 'state';
                        this.weather[i].state = 'normal';
                        console.log(this.weather[i].state);
                        }
                        
                        var g = document.querySelector('#gif');
                        g.remove();
                        });
                  
                  
                  },
                  });


